# What is the SimpleLogger? 

**SimpleLogger** is a project written in Node JS, MongoDB and angular 7 for simply using a REST API to add a log object and list this logs.

# How to use:
-You will need at least version 8 of node Js or higher.
-installing the gulp cli globally.
-installing the typescript globally.

# how to run the project
- 1 - navigate to `\server` and run `npm install or npm run setup`
- 2 - after that run `npm run build` then `npm start`
    >the server will run in this port `http://localhost:5000` and you can test the api throw swagger on `http://localhost:5000/swagger`
- 3 - for the front end navigate to `\client\SimpleLoggerApp` and run `npm install` then run `npm start or ng serve -o`

## Technologies implemented:

- Node js
- Hapi js
- MongoDB
- Angualr 7
- Swagger UI

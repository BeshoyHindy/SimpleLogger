import { Logging } from '../../../shared/models/Logging.Model';
import { LoggingService } from '../../../shared/services/Logging.Service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';


@Component({
    selector: 'app-logs-viewer-list',
    templateUrl: './logs-viewer-list.component.html',
    styleUrls: ['./logs-viewer-list.component.css']
})
export class LogsViewerListComponent implements OnInit {

    public searchText: string;
    logs: Logging[];

    constructor(
        private loggingService: LoggingService
    ) {

    }

    ngOnInit() {
        this.getLogs();

    }


    private getLogs(): void {
        this.loggingService
            .getAllLogs()
            .subscribe((res: Logging[]) => {
                //
                console.log(res);
                this.logs = res;
                console.log(this.logs);
            },
                err => {

                    console.log(err);
                });

    }


}

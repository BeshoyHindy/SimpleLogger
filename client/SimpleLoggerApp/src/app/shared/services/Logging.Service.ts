import { Logging } from '../models/Logging.Model';
import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class LoggingService {

  Logs: Logging[];

  readonly baseApiURL = 'http://localhost:5000/';
  readonly logUrl = this.baseApiURL + "log";

  constructor(private http: HttpClient) { }

  getAllLogs(pageCount: number = 10, pageIndex = 0) {
    let top = pageCount;
    let skip = pageCount * pageIndex
    return this.http.get<Logging[]>(`${this.logUrl}?top=${top}&skip=${skip}`);

  }

  getAllLogById(id: string) {
    return this.http.get<Logging>(`${this.baseApiURL}/${id}`);
  }
  createLog(log: Logging) {
    debugger
    return this.http.post(this.baseApiURL, log);
  }
}

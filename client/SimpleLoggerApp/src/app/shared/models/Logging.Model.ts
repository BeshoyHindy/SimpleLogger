export class Logging {  
    id: string;  
    title: string;
    description: string;
    statusCode: string;
    path: string;
    createdAt: Date;
}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { LogsViewerComponent } from './components/logs-viewer/logs-viewer.component';
import { LogsViewerListComponent } from './components/logs-viewer/logs-viewer-list/logs-viewer-list.component';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LoggingService } from './shared/services/Logging.Service';
import { TableFilterPipe } from './shared/pipes/table-filter.pipe';


@NgModule({
  declarations: [
    AppComponent,
    LogsViewerComponent,
    LogsViewerListComponent,
    TableFilterPipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,  
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [
    LoggingService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
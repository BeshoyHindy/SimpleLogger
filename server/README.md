# NodeJS-Hapi TypeScript Scaffolding

A NodeJS + HapiJS(17) with Typescript Starter kit to build standard projects.

**Installation**

* *npm run setup* (install nuget packages & typings)

**Important Note**

* If working with NodeJS 10.0.0, Kindly delete the *package.lock.json* file then try *npm install*

**Run**

* *gulp build* (build ts files)
* *gulp test* (run mocha tests)
* *gulp tslint* (run tslint)
* *gulp watch* (watch ts files)
* *npm run start* (start the application)
* *npm run watch* (restart the application when files change)

**Features**


Running on port 5000 ex: localhost:5000/swagger

Have fun :)

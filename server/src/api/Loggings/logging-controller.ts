import * as Hapi from "hapi";
import * as Boom from "boom";
import { ILogging } from "./logging";
import { IDatabase } from "../../database";
import { IServerConfigurations } from "../../configurations";
import { IRequest } from "../../interfaces/request";
// import { Guid } from "guid-typescript";

//Custom helper module
import * as Helper from "../../utils/helper";

export default class LoggingController {
  private database: IDatabase;
  private configs: IServerConfigurations;

  constructor(configs: IServerConfigurations, database: IDatabase) {
    this.configs = configs;
    this.database = database;
  }

  public async createLog(request: IRequest, h: Hapi.ResponseToolkit) {
    var newLog: ILogging = <ILogging>request.payload;
    // newLog.id = Guid.create().toString();

    try {
      let log: ILogging = await this.database.loggingModel.create(newLog);
      return h.response(log).code(201);
    } catch (error) {
      return Boom.badImplementation(error);
    }
  }



  public async getLogById(request: IRequest, h: Hapi.ResponseToolkit) {
    let _id = request.params["id"];

    let log = await this.database.loggingModel.findOne({ _id })
      .lean(true);

    if (log) {
      return log;
    } else {
      return Boom.notFound();
    }
  }

  public async getLogs(request: IRequest, h: Hapi.ResponseToolkit) {
    let top = request.query["top"];
    let skip = request.query["skip"];
    let logs = await this.database.loggingModel
      .find()
      .sort({createdAt: -1})
      .lean(true)
      .skip(skip)
      .limit(top);

    return logs;
  }
}

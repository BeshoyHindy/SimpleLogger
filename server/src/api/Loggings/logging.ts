import * as Mongoose from "mongoose";
import { Guid } from "guid-typescript";

export interface ILogging extends Mongoose.Document {
  title: string;
  description: string;
  statusCode: string;
  path: string;
  createdAt: Date;
}

export const LoggingSchema = new Mongoose.Schema(
  {
    title: { type: String, required: true },
    description: { type: String },
    statusCode: String,
    path: { type: String },
    createdAt: { type: Date }
  },
  {
    timestamps: true
  }
);

export const LoggingModel = Mongoose.model<ILogging>("Logging", LoggingSchema);

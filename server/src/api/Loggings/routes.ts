import * as Hapi from "hapi";
import * as Joi from "joi";
import LoggingController from "./logging-controller";
import * as LogValidator from "./logging-validator";

import { IDatabase } from "../../database";
import { IServerConfigurations } from "../../configurations";

export default function (
  server: Hapi.Server,
  configs: IServerConfigurations,
  database: IDatabase
) {
  const loggingController = new LoggingController(configs, database);
  server.bind(loggingController);

  server.route({
    method: "GET",
    path: "/log/{id}",
    options: {
      handler: loggingController.getLogById,
      tags: ["api", "log"],
      description: "Get Log item by id.",
      validate: {
        params: {
          id: Joi.string().required()
        }
      },
      plugins: {
        "hapi-swagger": {
          responses: {
            "200": {
              description: "Log item founded."
            },
            "404": {
              description: "log does not exists."
            }
          }
        }
      }
    }
  });

  server.route({
    method: "GET",
    path: "/log",
    options: {
      handler: loggingController.getLogs,
      tags: ["api", "log"],
      description: "Get all logs.",
      validate: {
        query: {
          top: Joi.number().default(5),
          skip: Joi.number().default(0)
        }
      }
    }
  });

   server.route({
    method: "POST",
    path: "/log",
    options: {
      handler: loggingController.createLog,
      tags: ["api", "log"],
      description: "Create a log.",
      validate: {
        payload: LogValidator.createLogModel,
      },
      plugins: {
        "hapi-swagger": {
          responses: {
            "201": {
              description: "Created Log."
            }
          }
        }
      }
    }
  });
}

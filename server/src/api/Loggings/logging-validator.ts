import * as Joi from "joi";

export const createLogModel = Joi.object().keys({
 // title: Joi.string().required(),
  title: Joi.string(),
  description: Joi.string(),
  statusCode: Joi.string(),
  path: Joi.string(),
  createdAt: Joi.date()
});




import * as Hapi from "hapi";


export interface IRequest extends Hapi.Request {


}

export interface ILoginRequest extends IRequest {
  payload: {
    id: string;
    title: string;
    description: string;
    statusCode: string;
    path: string;
    createdAt: Date;
  };
}
